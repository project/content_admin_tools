<?php

namespace Drupal\text_replace;

use Drupal\Component\Utility\Html;
use Drupal\text_replace\Controller\Controller;

/**
 * This class is in charge of the node alias updates.
 *
 * @package Drupal\text_replace
 */
class NodeUpdater extends Controller {

  /**
   * Batch operation implementation
   *
   * @param $nid
   * @param $langCode
   * @param $field
   * @param $currentString
   * @param $replacementString
   * @param $context
   */
  public static function batchNodeUpdater($nid, $langCode, $field, $currentString, $replacementString, &$context) {

    $replace_type_chosen = \Drupal::service('user.private_tempstore')
      ->get('text_replace')
      ->get('replace_type');

    $title = self::getNodeByNidLangCode($nid, $langCode)->get('title')->value;

    // Optional message displayed under the progressbar.
    $context['message'] = t('Updating node <b>@nid</b> with title: <i>@title</i> for field <i>@field</i>',
      [
        '@nid' => Html::escape($nid),
        '@title' => Html::escape($title),
        '@field' => Html::escape($field),
      ]
    );

    // Here we actually perform our dummy 'processing' on the current node.
    usleep(20000);
    self::updateFieldNode($nid, $langCode, $field, $currentString, $replacementString, $replace_type_chosen);
    \Drupal::logger('text_replace')
      ->notice('Replaced <i>@current-string</i> with <i>@replace-text</i> for field <i>@field</i> on node <i>@title</i> (@nid)', [
        '@current-string' => $currentString,
        '@replace-text' => $replacementString,
        '@field' => $field,
        '@title' => $title,
        '@nid' => $nid,
      ]);

    if (!isset($context['results']['updated']) || !$context['results']['updated']) {
      $context['results']['updated'] = 1;
      $context['results']['node_updated'] = 1;
    }
    else {
      $context['results']['updated']++;
      $context['results']['node_updated']++;
    }
  }

  public static function batchBulkNodeUpdater($nid, $strings, $maxOperations, &$context) {
  /*  // Save node count for the termination message.
    $context['sandbox']['max'] = self::getPreProcessMax();*/

    // Use the $context['sandbox'] at your convenience to store the
    // information needed to track progression between successive calls.
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
     // $context['sandbox']['current_node'] = 0;

      // Save node count for the termination message.
      $context['sandbox']['max'] = $maxOperations;
    }
    $user = \Drupal::currentUser();
    $replace_type_chosen = \Drupal::service('user.private_tempstore')
      ->get('text_replace')
      ->get('replace_type');

    $selected_fields_results = \Drupal::service('user.private_tempstore')
      ->get('text_replace')
      ->get('selected_fields_results');

    //    $selected_fields_results = $_SESSION['text_replace.selected_fields_results'];

    $preProcess = [];

    $entity = self::getEntityNodeByNid($nid);
    foreach ($selected_fields_results as $field) {

      $title = $entity->get('title')->value;

      $mixed_value = explode('.', $field);
      // Optional message displayed under the progressbar.
      $context['message'] = t("Searching node <b>@nid</b> with title: <i>@title</i> for field <i>@field</i> ",
        [

          '@nid' => Html::escape($nid),
          '@title' => Html::escape($title),
          '@field' => Html::escape($mixed_value[2]),
        ]
      );
      $results = self::searchCurrentString2($entity, $mixed_value[2], $strings, $replace_type_chosen);
      if (is_array($results)) {
        foreach ($results as $langCode => $result) {
          $preProcess[$mixed_value[2]][$langCode] = $result;
        }
      }

      foreach ($preProcess as $field => $languages) {

        foreach ($languages as $language => $ocurrences) {
          if (count($ocurrences) > 0) {
            $entityTranslation = $entity->getTranslation($language);
            $fieldValue = $entityTranslation->get($field)->value;
            $title = $entityTranslation->get('title')->value;
            foreach ($ocurrences as $ocurrence) {

              $fieldValue = self::updateEntityField($fieldValue, $ocurrence, $strings[$ocurrence], $replace_type_chosen);

              \Drupal::logger('text_replace')
                ->notice('Replaced <i>@current-string</i> with <i>@replace-text</i> for field <i>@field</i> on node <i>@title</i> (@nid) with language <i>@langCode</i>', [
                  '@current-string' => Html::escape($ocurrence),
                  '@replace-text' => Html::escape($strings[$ocurrence]),
                  '@field' => Html::escape($field),
                  '@title' => Html::escape($title),
                  '@nid' => Html::escape($nid),
                  '@langCode' => Html::escape($language),
                ]);

              // Here we actually perform our dummy 'processing' on the current node.
              usleep(20000);
              // Make this change a new revision
              $entityTranslation->setNewRevision(TRUE);
              $entityTranslation->setRevisionLogMessage(t('Replaced <i>@current-string</i> with <i>@replace-text</i> for field <i>@field</i> on node <i>@title</i> (@nid) with language <i>@langCode</i>', [
                '@current-string' => Html::escape($ocurrence),
                '@replace-text' => Html::escape($strings[$ocurrence]),
                '@field' => Html::escape($field),
                '@title' => Html::escape($title),
                '@nid' => Html::escape($nid),
                '@langCode' => Html::escape($language),
              ]));
              $entityTranslation->setRevisionCreationTime(REQUEST_TIME);
              $entityTranslation->setRevisionUserId($user->id());


            }

            if ($field == 'body') {
              $entityTranslation->set($field, [
                'value' => $fieldValue,
                'format' => 'padi_full_markup',
              ]);
            }
            else {
              $entityTranslation->set($field, $fieldValue);
            }

            $entityTranslation->save();

            if (!isset($context['results']['node_updated']) || !$context['results']['node_updated']) {
              $context['results']['node_updated'] = 1;
            }
            else {
              $context['results']['node_updated']++;
            }
            // Optional message displayed under the progressbar.
            $context['message'] = t('Updating node <b>@nid</b> with title: <i>@title</i> for field <i>@field</i> with language <i>@langCode</i>',
              [
                '@nid' => Html::escape($nid),
                '@title' => Html::escape($title),
                '@field' => Html::escape($mixed_value[2]),
                '@field' => Html::escape($field),
                '@langCode' => Html::escape($language),
              ]
            );
          }
        }
      }
    }

    if (!isset($context['results']['updated']) || !$context['results']['updated']) {
      $context['results']['updated'] = 1;
    }
    else {
      $context['results']['updated']++;
    }

    $context['sandbox']['progress']++;
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']);
    }
  }

  /**
   * Callback for batch finished.
   *
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function batchFinishedCallback($success, $results, $operations) {
    $node_updated = 0;
    if (isset($results['node_updated'])) {
      $node_updated = $results['node_updated'];
    }
    if ($success) {
      // drupal_set_message($results['updated'] . " nodes where succesfully set.");
      drupal_set_message(\Drupal::translation()
        ->translate('@results processed nodes, @node_updated nodes were successfully updated.', [
          '@results' => $results['updated'],
          '@node_updated' => $node_updated,
        ]));
    }
    else {
      //drupal_set_message(count($operations) . " nodes failed to update.", "error");
      drupal_set_message(\Drupal::translation()
        ->translate('@operations nodes failed to update.', ['@operations' => count($operations)]), "error");
    }
  }
}