<?php

/**
 * @file
 * Contains \Drupal\example_module\Form\ConfirmDeleteForm.
 */

namespace Drupal\text_replace\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\NodeType;
use Drupal\text_replace\Controller\Controller;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ConfirmUpdateForm extends ConfirmFormBase {

  /**
   * The tempstore controller.
   *
   * @var \Drupal\text_replace\Controller\Controller
   */
  protected $controller;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $manager;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;


  /**
   * Constructs a DeleteMultiple form object.
   *
   * @param  \Drupal\text_replace\Controller\Controller $controller
   *    The controller manager.
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface $manager
   *   The entity manager.
   */
  public function __construct(Controller $controller, PrivateTempStoreFactory $temp_store_factory, EntityManagerInterface $manager) {
    $this->controller = $controller;
    $this->tempStoreFactory = $temp_store_factory;
    $this->storage = $manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('text_replace.controller'),
      $container->get('user.private_tempstore'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {


    if ($this->tempStoreFactory->get('text_replace')->get('active')) {

      // Getting information from form.

      $contentTypes = $this->tempStoreFactory->get('text_replace')
        ->get('contentTypes');
      $fieldsSelected = $this->tempStoreFactory->get('text_replace')
        ->get('selected_fields_results');

      $contentTypeFields = [];

      foreach ($fieldsSelected as $fieldSelected) {
        $process = explode('.', $fieldSelected);
        $contentTypeFields[$process[1]][] = $process[2];
      }

      $items = $this->controller->buildItemsForConfirmForm($contentTypeFields);

      $form['nodes'] = [
        '#theme' => 'item_list',
        '#items' => $items,
      ];


      $form['contacts'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Nid'),
          $this->t('Content Type'),
          $this->t('Title'),
        ],
      ];

      foreach ($contentTypes as $contentType) {
        $nids[$contentType] = $this->controller->getNodeList($contentType);
        $nodes[$contentType] = $this->storage->loadMultiple(array_values($nids[$contentType]));
        foreach ($nodes[$contentType] as $i => $node) {
          $form['contacts'][$i]['#attributes'] = [
            'class' => [
              'foo',
              'baz',
            ],
          ];
          $form['contacts'][$i]['nid'] = [
            '#type' => 'item',
            '#title' => $node->id(),
          ];
          $form['contacts'][$i]['content_type'] = [
            '#type' => 'item',
            '#title' => NodeType::load($node->getType())->label(),
          ];
          $form['contacts'][$i]['title'] = [
            '#type' => 'item',
            '#title' => $node->label(),
          ];
        }
      }

    }
    else {

      // Getting information from form.

      $elementsSelected = $this->tempStoreFactory->get('text_replace')
        ->get('elementsSelected');
      $strings = $this->tempStoreFactory->get('text_replace')
        ->get('strings');

      $form['contacts'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Nid'),
          $this->t('Title'),
          $this->t('Language'),
          $this->t('Content Type'),
          $this->t('Field'),
          $this->t('Current string'),
          $this->t('String replacement'),
        ],
      ];

      foreach ($elementsSelected as $i => $element) {
        if ($element != 0) {
          $process = explode('.', $element);
          $NodeEntity = $this->controller->getEntityNodeByNid($process[0]);
          $form['contacts'][$i]['#attributes'] = [
            'class' => [
              'foo',
              'baz',
            ],
          ];
          $form['contacts'][$i]['nid'] = [
            '#type' => 'item',
            '#title' => $NodeEntity->id(),
          ];
          $form['contacts'][$i]['title'] = [
            '#type' => 'item',
            '#title' => $NodeEntity->label(),
          ];
          $form['contacts'][$i]['language'] = [
            '#type' => 'item',
            '#title' => $process[1],
          ];
          $form['contacts'][$i]['content_type'] = [
            '#type' => 'item',
            '#title' => NodeType::load($NodeEntity->getType())->label(),
          ];
          $form['contacts'][$i]['field'] = [
            '#type' => 'item',
            '#title' => $process[2],
          ];
          $form['contacts'][$i]['currentString'] = [
            '#type' => 'item',
            '#title' => $strings['currentString'],
          ];
          $form['contacts'][$i]['stringReplacement'] = [
            '#type' => 'item',
            '#title' => $strings['stringReplacement'],
          ];
        }
      }

    }


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    //Config batch
    $batch = [
      'title' => t('Processing nodes...'),
      'operations' => [],
      'finished' => '\Drupal\text_replace\NodeUpdater::batchFinishedCallback',
      'init_message' => t('The update job is starting...'),
      'progress_message' => t('Completed @current of @total.'),
      'error_message' => t('The updater encountered an error and couldn\'t finish'),
    ];


    if ($this->tempStoreFactory->get('text_replace')->get('active')) {

      // Getting information from form.

      $data = $this->tempStoreFactory->get('text_replace')
        ->get('contentTypes');

      $maxOperations = $this->controller->getPreProcessMax($data);
      $file = $this->tempStoreFactory->get('text_replace')->get('my_file');
      $uri = File::load($file)->getFileUri();
      $handle = fopen($uri, 'r');
      $strings = [];
      while (!feof($handle)) {
        $row = fgetcsv($handle);
        $strings[$row[0]] = $row[1];
      }
      //Generate operations
      foreach ($data as $item) {
        foreach ($this->controller->getNodeList($item) as $nid) {
          $batch['operations'][] = [
            '\Drupal\text_replace\NodeUpdater::batchBulkNodeUpdater',
            [$nid, $strings, $maxOperations],
          ];
        }
      }



    }
    else {

      // Getting information from form.

      $elementsSelected = $this->tempStoreFactory->get('text_replace')
        ->get('elementsSelected');
      $strings = $this->tempStoreFactory->get('text_replace')
        ->get('strings');


    //Generate operations
      foreach ($elementsSelected as $element) {
        if ($element != 0) {
          $mixElement = explode('.', $element);
          $batch['operations'][] = [
            '\Drupal\text_replace\NodeUpdater::batchNodeUpdater',
            [
              $mixElement[0],
              $mixElement[1],
              $mixElement[2],
              $strings['currentString'],
              $strings['stringReplacement'],
            ],
          ];
        }
      }

    }


    //Set batch up
    batch_set($batch);


    // Reset save temp data.
/*    $this->tempStoreFactory->get('text_replace')->set('active', NULL);
    // $this->tempStoreFactory->get('text_replace')->set('contentTypes', NULL);
    // Reset save temp data.
    $this->tempStoreFactory->get('text_replace')->set('contentTypes', NULL);
    $this->tempStoreFactory->get('text_replace')->set('selected_fields_results', NULL);
    $this->tempStoreFactory->get('text_replace')->set('my_file', NULL);
    // Reset save temp data.
    $this->tempStoreFactory->get('text_replace')->set('elementsSelected', NULL);
    $this->tempStoreFactory->get('text_replace')->set('strings', NULL);*/

    $form_state->setRedirect('text_replace.admin_settings');

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "confirm_update_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('text_replace.admin_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $contentTypes = $this->tempStoreFactory->get('text_replace')->get('contentTypes');
    $contentTypelist = count($contentTypes) > 1 ? $this->controller->convertStringContentTypes($contentTypes) : NodeType::load(array_values($contentTypes)[0])->label();
    return t('Do you want to update the content types %id?', ['%id' => Html::escape($contentTypelist)]);
  }

}