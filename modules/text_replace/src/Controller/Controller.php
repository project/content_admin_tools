<?php

namespace Drupal\text_replace\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\NodeType;
use Drupal\Component\Utility\Html;

/**
 * This class is in charge of the node alias updates.
 *
 * @package Drupal\text_replace
 */
class Controller extends ControllerBase {


  public static function getNodeList($type) {
    return \Drupal::entityQuery('node')
      ->condition('type', "{$type}")
      ->execute();
  }

  public static function getFieldListByContentType($type) {
    $entityManager = \Drupal::service('entity.manager');
    if (self::fieldInConfiguration($entityManager->getFieldDefinitions('node', $type))) {
      return [
        $type => self::fieldInConfiguration($entityManager->getFieldDefinitions('node', $type)),
      ];
    }
  }

  public static function fieldInConfiguration($field_definitions) {
    $fieldsFilter = ['string', 'text_with_summary', 'text_long'];
    $fields = [];
    foreach ($field_definitions as $field) {
      if ($field instanceof FieldConfig && in_array($field->getType(), $fieldsFilter)) {
        $fields[] = $field;
      }
    }
    return $fields;
  }

  public function getContentTypes() {
    return \Drupal::service('entity.manager')
      ->getStorage('node_type')
      ->loadMultiple();
  }

  public function getContentTypeByNameMachine($contentTypeNameMachine) {
    return self::getContentTypes()[$contentTypeNameMachine];
  }

  public function getNodeTypeLoadId($contentTypeId) {
    return NodeType::load($contentTypeId);
  }

  public function convertStringContentTypes($contentTypes) {
    $result = [];
    foreach ($contentTypes as $contentType) {
      $result[] = NodeType::load($contentType)->label();
    }
    return implode(', ', $result);
  }

  public static function getNodeByNidLangCode($nid, $langCode) {
    $entity = self::getEntityNodeByNid($nid);
    return $entity->getTranslation($langCode);
  }

  public static function getEntityNodeByNid($nid) {
    return \Drupal::entityTypeManager()->getStorage('node')->load($nid);
  }

  public static function searchString($nid, $field, $currentString, $replace_type_chosen) {
    $entity = self::getEntityNodeByNid($nid);
    $languages = $entity->getTranslationLanguages();
    $results = [];

    //Explore translations
    foreach ($languages as $lang_code => $language) {
      $entity_translation = $entity->getTranslation($lang_code);
      if ($entity_translation->hasField($field)) {
        //  $value = $entity_translation->get($field)->getValue()[0]['value'];
        $value = $entity_translation->get($field)->value;
        /* if ($lang_code == 'en' && $nid == 1971){
           dpm($nid);
           dpm($lang_code);
           dpm($field);
           dpm($currentString);
           dpm($replace_type_chosen);
           dpm($value);
           dpm(self::processRegex($currentString, NULL, $value, FALSE, $replace_type_chosen));
         }*/

        if (self::processRegex($currentString, NULL, $value, FALSE, $replace_type_chosen)) {
          // if (strpos($value, $currentString) != FALSE) {
          $results[$field][$lang_code] = [
            'nid' => $nid,
            'field' => $field,
            'currentString' => $currentString,
            'lang_code' => $lang_code,
          ];
        }
      }
    }
    if (!empty($results)) {
      return $results;
    }


    return FALSE;
  }

  public function searchCurrentString($nid, $field, $currentString) {
    $entity = self::getEntityNodeByNid($nid);
    $languages = $entity->getTranslationLanguages();
    $results = [];

    //Explore translations
    foreach ($languages as $lang_code => $language) {
      $entity_translation = $entity->getTranslation($lang_code);
      if ($entity_translation->hasField($field)) {
        $value = $entity_translation->get($field)->getValue()[0]['value'];
        if (strpos($value, $currentString) != FALSE) {
          //$results[$field] = [
          $results[] = [
            'lang_code' => $lang_code,
            'currentString' => $currentString,
          ];

        }
      }
    }
    if (!empty($results)) {
      return $results;
    }
    return FALSE;
  }

  public static function searchCurrentString2($entity, $field, $strings, $replace_type_chosen) {
    $languages = $entity->getTranslationLanguages();
    $results = [];

    //Explore translations
    foreach ($languages as $lang_code => $language) {
      $entity_translation = $entity->getTranslation($lang_code);
      if ($entity_translation->hasField($field)) {
        $value = $entity_translation->get($field)->value;
        foreach (array_keys($strings) as $currentString) {
          if (self::processRegex($currentString, NULL, $value, FALSE, $replace_type_chosen)) {
            $results[$lang_code][] = $currentString;
          }
        }

      }
    }
    if (!empty($results)) {
      return $results;
    }
    return FALSE;
  }

  public static function updateFieldNode($nid, $langCode, $field, $currentString, $replacementString, $replace_type_chosen) {
    $user = \Drupal::currentUser();
    $entity = self::getEntityNodeByNid($nid);
    $entityTranslation = $entity->getTranslation($langCode);
    $fieldValue = $entityTranslation->get($field)->value;
    //$changeFieldValue = str_replace($currentString, $replacementString, $fieldValue);
    $changeFieldValue = self::processRegex($currentString, $replacementString, $fieldValue, TRUE, $replace_type_chosen);

    // $entityTranslation->set($field, $changeFieldValue);
    if ($field == 'body') {
      $entityTranslation->set($field, [
        'value' => $changeFieldValue,
        'format' => 'padi_full_markup',
      ]);
    }
    else {
      $entityTranslation->set($field, $changeFieldValue);
    }
    // Set revision to false will prevent it get errors.
    // Make this change a new revision
    $entityTranslation->setNewRevision(TRUE);
    $entityTranslation->setRevisionLogMessage(t('Replaced <i>@current-string</i> with <i>@replace-text</i> for field <i>@field</i> on node <i>@title</i> (@nid) with language <i>@langCode</i>', [
      '@current-string' => Html::escape($currentString),
      '@replace-text' => Html::escape($replacementString),
      '@field' => Html::escape($field),
      '@title' => Html::escape($entityTranslation->get('title')->value),
      '@nid' => Html::escape($nid),
      '@langCode' => Html::escape($langCode),
    ]));
    $entityTranslation->setRevisionCreationTime(REQUEST_TIME);
    $entityTranslation->setRevisionUserId($user->id());
    // $entityTranslation->setNewRevision(FALSE);
    $entityTranslation->save();
  }

  public static function updateEntityField($fieldValue, $ocurrence, $replacementString, $replace_type_chosen) {

    $changeFieldValue = self::processRegex($ocurrence, $replacementString, $fieldValue, TRUE, $replace_type_chosen);


    return $changeFieldValue;
  }

  public static function processRegex($string_search, $string_replace, $field_value, $update, $replace_type_chosen) {
    //  $string_search = 'dive'; // palabra a buscar
    //  $string_replace = 'bucear'; // remplazar por esta
    //  $string_context = 'When you learn to dive, you become a diver. Enter here to <a href="http://www.padi.com/fr/course/masks">find more</a> or <a href="/de/course/snorchel">here</a>'; // valor del campo

    // quoted_boundaries = Busca en strings delimitados por "" (quotes), como por ejemplo un link u otro.
    // words_only = Busca en strings delimitados por espacios (solo palabras enteras), como por ejemplo: "dive" no va a encontrar nada en "diver".
    // none = Busca en strings sin restriccion alguna, como por ejemplo: "dive" va a encontrarse en "diver".
    $replace_type = [
      2 => 'quoted_boundaries',
      1 => 'words_only',
      0 => 'none',
    ]; // Este valor traes de la configuracion/paso 3

    $regex = '';
    $string_search = str_replace('/', '\/', $string_search);

    switch ($replace_type[$replace_type_chosen]) {
      case 'quoted_boundaries':
        $regex = '/["](' . $string_search . ')["?]/i';
        $string_replace = "\"".$string_replace."\"";
        break;
      case 'words_only':
        $regex = '/\b(' . $string_search . ')\b/';
        break;
      case 'none':
        $regex = '/(' . $string_search . ')/';
        break;
    }

    if ($update) {
      return preg_replace($regex, $string_replace, $field_value);
    }
    else {
      preg_match($regex, $field_value, $matches);
      return count($matches) > 0 ? TRUE : FALSE;
    }
  }

  public function buildItemsForConfirmForm($contentTypeFields) {
    $items = [];
    foreach ($contentTypeFields as $contentType => $fields) {
      $key = $contentType . ':' . $contentType;
      $default_key = $contentType . ':' . $key;

      $names = [];
      foreach ($fields as $key => $field) {
        $names[] = $field;
        unset($items[$contentType . ':' . $key]);
      }
      $items[$default_key] = [
        'label' => [
          '#markup' => $this->t('@label (@contentType) - <em> In the following fields will be searched and updated if you find occurrences :</em>', [
            '@label' => NodeType::load($contentType)
              ->label(),
            '@contentType' => $contentType,
          ]),
        ],
        'deleted_translations' => [
          '#theme' => 'item_list',
          '#items' => $names,
        ],
      ];
    }
    return $items;
  }

  public function preProcessFieldsByContentType($arrayFields) {
    $results = [];
    foreach ($arrayFields as $key => $field) {
      $elements = explode('.', $field);
      $results[$elements[1]][] = $elements[2];
    }
    return $results;
  }

  public static function getPreProcessMax($data) {
    // Getting information from form.
    $max = 0;
    //Generate operations
    foreach ($data as $item) {
      $max += count(self::getNodeList($item));
      /* foreach ($this->controller->getNodeList($item) as $nid) {
       * $batch['operations'][] = [
           '\Drupal\text_replace\NodeUpdater::batchBulkNodeUpdater',
           [$nid, $strings],
         ];
       }*/
    }
    return $max;

  }
}