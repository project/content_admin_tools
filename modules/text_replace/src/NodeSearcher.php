<?php

namespace Drupal\text_replace;

use Drupal\Component\Utility\Html;
use Drupal\node\Entity\NodeType;
use Drupal\text_replace\Controller\Controller;

/**
 * This class is in charge of the candidate node detection operations.
 *
 * @package Drupal\text_replace
 */
class NodeSearcher extends Controller {


  public static function batchFieldsNodeInspector($contentType, &$context) {

    // Optional message displayed under the progressbar.
    $context['message'] = t('Scanning content type @contentTypeLabel with id "@contentTypeId" ',
      [
        '@contentTypeId' => Html::escape($contentType),
        '@contentTypeLabel' => Html::escape(NodeType::load($contentType)
          ->label()),
      ]
    );
    $context['results'][] = self::getFieldListByContentType($contentType);
  }

  /**
   * Implements function batchNodeInspector().
   *
   * @param mixed $data
   *   The data variable.
   * @param mixed $searchString
   *   The searchString variable.
   * @param mixed $context
   *   The context variable.
   */
  public static function batchNodeInspector($data, $searchString, &$context) {
    //$selected_fields_results = $_SESSION['text_replace.selected_fields_results'];
    $selected_fields_results = \Drupal::service('user.private_tempstore')->get('text_replace')->get('selected_fields_results');
    $replace_type_chosen = \Drupal::service('user.private_tempstore')
      ->get('text_replace')
      ->get('replace_type');

    $list = [];
    $nodeList = self::getNodeList($data);
    foreach ($nodeList as $nid) {
      foreach ($selected_fields_results as $field) {
        $mixed_value = explode('.', $field);
        if (in_array($data, $mixed_value)) {
          $result = self::searchString($nid, $mixed_value[2], $searchString, $replace_type_chosen);
          if ($result != FALSE) {
            $list[$nid] = $result;
          }
        }
      }
      $nodeLabel = self::getEntityNodeByNid($nid)->label();

      // Optional message displayed under the progressbar.
      $context['message'] = t('Scanning node @nodeLabel with id "@nodeId" and field @field',
        [
          '@nodeId' => Html::escape($nid),
          '@nodeLabel' => Html::escape($nodeLabel),
          '@field' => Html::escape($mixed_value[2]),
        ]
      );

    }

    $context['results'][$data] = $list;

  }

  /**
   * Callback for batch finished.
   *
   * @param mixed $success
   *   The success variable.
   * @param mixed $results
   *   The results variable.
   * @param mixed $operations
   *   The operations variable.
   */
  public static function batchFinishedCallback($success, $results, $operations) {
    // Stores the batch results in the session so can be picked up at the form.
    \Drupal::service('user.private_tempstore')
      ->get('text_replace')
      ->set('batch_results', [
        'success' => $success,
        'results' => (!is_array($results)) ? [] : ($results),
        // Ensure the result is always an array.
        'operations' => $operations,
      ]);

  /*  $_SESSION['text_replace.batch_results'] = [
      'success' => $success,
      'results' => (!is_array($results)) ? [] : ($results),
      // Ensure the result is always an array.
      'operations' => $operations,
    ];*/
  }

 /* function _batch_text_replace_get_http_requests() {
    return !empty($_SESSION['http_request_count']) ? $_SESSION['http_request_count'] : 0;
  }*/

}
