<?php

namespace Drupal\text_replace\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text_replace\Controller\Controller;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Multistep form implementing the interface for the functionality.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MultistepForm extends FormBase {

  /**
   * The tempstore controller.
   *
   * @var \Drupal\text_replace\Controller\Controller
   */
  protected $controller;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a DeleteMultiple form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(Controller $controller, PrivateTempStoreFactory $temp_store_factory) {
    $this->controller = $controller;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('text_replace.controller'),
      $container->get('user.private_tempstore')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'text_replace';
  }

  /**
   * Works as a router for the form steps.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //Choose page to show
    if ($form_state->has('page_num')) {
      switch ($form_state->get('page_num')) {
        case 1:
          return $this->buildPageOne($form, $form_state);
          break;
        case 2:
          return $this->buildPageTwo($form, $form_state);
          break;
        case 3:
          return $this->buildPageThree($form, $form_state);
          break;
        case 4:
          return $this->buildPageFour($form, $form_state);
          break;
      }
    }

    //Fallback to the first page
    $form_state->set('page_num', 1);
    return self::buildPageOne($form, $form_state);
  }

  /**
   * Builder of the first page.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildPageOne(array $form, FormStateInterface $form_state) {
   /* $string_replace = '/courses/tec-40-ccr-instructor';
    dpm("\"".$string_replace."\"");*/

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Select content types to search for.'),
    ];
    $content_types = $form_state->getValue('content_types', []);

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getOptionsCheckbox(),
      '#required' => TRUE,
      '#title' => $this->t('Content type'),
      '#default_value' => isset($content_types) ? $content_types : [],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Search'),
      '#submit' => ['::submitPageOne'],
    ];

    //Clears unused memory.
    $form_state->set('pre_processed_nodes', NULL);

    return $form;
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitPageOne(array &$form, FormStateInterface $form_state) {
    $list_content_type = $this->processCheckboxSelected($form_state->getValue('content_types'));

    //Config batch
    $batch = [
      'title' => t('Scanning content types...'),
      'operations' => [],
      'finished' => '\Drupal\text_replace\NodeSearcher::batchFinishedCallback',
      'init_message' => t('The scanning is starting...'),
      'progress_message' => t('Completed @current of @total.'),
      'error_message' => t('The scanner encountered an error and couldn\'t finish'),
    ];


    // Generate operations
    foreach ($list_content_type as $value) {
      $batch['operations'][] = [
        '\Drupal\text_replace\NodeSearcher::batchFieldsNodeInspector',
        [$value],
      ];
    }

    //Set batch up
    batch_set($batch);

    //Cache in case user decides to go back
    $form_state->set('page_values', [
      'content_types' => $form_state->getValue('content_types'),
    ]);

    $this->moveNextPage($form_state);
  }

  /**
   * Builds the second step form (page 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildPageTwo(array &$form, FormStateInterface $form_state) {
    $form['#cache'] = FALSE;

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Select the fields that will be included in the search.'),
    ];

    $header = [
      'bundle' => t('Bundle'),
      'fid' => t('Id'),
      //'title' => t('Title'),
      'field_name' => t('Field name'),
      'field_type' => t('Field type'),
      'label' => t('Label'),
      'required' => t('Required'),

    ];

    $data = $this->tempStoreFactory->get('text_replace')->get('batch_results')['results'];
   // $data = $_SESSION['text_replace.batch_results']['results'];
    $pre_processed = $this->buildTableData2($data);


    //Preserve preprocessed so we speed up submit handling.
    $form_state->set('pre_processed_nodes', $pre_processed);
    //Clean unused memory.
    $form_state->set('batch', NULL);

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $pre_processed,
      '#empty' => t('No suitable nodes found.'),
    ];

    $form['selected_fields'] = [
      '#type' => 'value',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      // Custom submission handler for 'Back' button.
      '#submit' => ['::submitBackButton'],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      '#submit' => ['::submitPageTwo'],
    ];

    if (empty($pre_processed)) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitPageTwo(array &$form, FormStateInterface $form_state) {
    $selected_fields_results = $form_state->getValue('table');
    foreach ($selected_fields_results as $key => $item) {
      if (($item == '0')) {
        unset($selected_fields_results[$key]);
      }
    }
    $this->tempStoreFactory->get('text_replace')->set('selected_fields_results', $selected_fields_results);
    //$_SESSION['text_replace.selected_fields_results'] = $selected_fields_results;
    $form_state->set('selected_fields_results', $selected_fields_results);

    $this->moveNextPage($form_state);
  }

  /**
   * Builds the second step form (page 3).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildPageThree(array &$form, FormStateInterface $form_state) {
    $form['#cache'] = FALSE;

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Please select the configuration for the text replacer.'),
    ];

    //Clean unused memory.
    $form_state->set('batch', NULL);


    $form['replace_type'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Replace type'),
      '#default_value' => 1,
      '#options' => [
        0 => $this->t('None'),
        1 => $this->t('Words only'),
        2 => $this->t('Links'),
      ],
      0 => [
        '#description' => t('Search strings without restriction, such as: "dive" will be in "diver".'),
      ],
      1 => [
        '#description' => t('Search in strings delimited by spaces (only whole words), such as: "dive" will not find anything in "diver".'),
      ],
      2 => [
        '#description' => t('Search strings delimited by "" (quotes), such as a link or another.'),
      ],
    ];

    $form['active'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Source'),
      '#default_value' => 0,
      '#options' => [
        0 => $this->t('Enter a search and text to replace.'),
        1 => $this->t('Upload CSV file'),
      ],
    ];


    // The #states property used here binds the visibility of the of the
    // container element to the value of the needs_accommodation checkbox above.
    $form['search_and_replace_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'search_and_replace',
      ],
      '#states' => [
        'invisible' => [
          'input[name="active"]' => ['value' => 1],
        ],
      ],
    ];

   /* $form['search_and_replace_container']['text_processor'] = [
      '#type' => 'fieldset',
      //'#title' => $this->t('Author'),
    ];*/




    $form['search_and_replace_container']['current_string'] = [
      '#type' => 'textfield',
      '#title' => t('Current string'),
      // '#default_value' => $node->title,
      '#size' => 60,
      '#maxlength' => 128,
      //'#required' => TRUE,
      '#states' => [
        'required' => [
          'input[name="active"]' => ['value' => 0],
        ],
      ],
    ];

    $form['search_and_replace_container']['string_replacement'] = [
      '#type' => 'textfield',
      '#title' => t('String replacement'),
      // '#default_value' => $node->title,
      '#size' => 60,
      '#maxlength' => 128,
      //  '#required' => TRUE,
      '#states' => [
        'required' => [
          'input[name="active"]' => ['value' => 0],
        ],
      ],
    ];

    // The #states property used here binds the visibility of the of the
    // container element to the value of the needs_accommodation checkbox above.
    $form['file_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'file_container',
      ],
      '#states' => [
        'invisible' => [
          'input[name="active"]' => ['value' => 0],
        ],
      ],
    ];

    $validators = [
      'file_validate_extensions' => ['csv'],
    ];

    $form['file_container']['my_file'] = [
      '#type' => 'managed_file',
      '#name' => 'my_file',
      '#title' => t('File *'),
      '#size' => 20,
      //  '#required' => TRUE,
      '#description' => t('CSV format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://my_files/',
    ];


    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      // Custom submission handler for 'Back' button.
      '#submit' => ['::submitBackButton'],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Search'),
      '#submit' => ['::submitPageThree'],
    ];

    return $form;
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitPageThree(array &$form, FormStateInterface $form_state) {


    $data = [];

    foreach ($form_state->get('selected_fields_results') as $item) {
      $item_array = explode('.', $item);
      $data[$item_array[1]] = $item_array[1];
    }

    $this->tempStoreFactory->get('text_replace')
      ->set('active', $form_state->getValue('active'));
    $this->tempStoreFactory->get('text_replace')
      ->set('replace_type', $form_state->getValue('replace_type'));

    if ($form_state->getValue('active')) {
      $this->tempStoreFactory->get('text_replace')
        ->set('contentTypes', $data);
      $this->tempStoreFactory->get('text_replace')
        ->set('my_file', $form_state->getValue('my_file')[0]);
      $form_state->setRedirect('text_replace.confirm_update_form');
      return;

    }
    else {
      //Config batch
      $batch = [
        'title' => t('Searching nodes...'),
        'operations' => [],
        'finished' => '\Drupal\text_replace\NodeSearcher::batchFinishedCallback',
        'init_message' => t('The update job is starting...'),
        'progress_message' => t('Completed @current of @total.'),
        'error_message' => t('The updater encountered an error and couldn\'t finish'),
      ];

      $searchString = $form_state->getValue('current_string');
      $form_state->set('string_replacement', $form_state->getValue('string_replacement'));
      $form_state->set('current_string', $form_state->getValue('current_string'));


      //Generate operations
      foreach ($data as $item) {
        $batch['operations'][] = [
          '\Drupal\text_replace\NodeSearcher::batchNodeInspector',
          [$item, $searchString],
        ];
      }

      //Set batch up
      batch_set($batch);
    }

    $this->moveNextPage($form_state);
  }


  /**
   * Builds the second step form (page 4).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildPageFour(array &$form, FormStateInterface $form_state) {

    $form['#cache'] = FALSE;

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('The following nodes are ready for update.'),
    ];

    $header = [
      'nid' => t('Node'),
      'title' => t('Title'),
      'lang_code' => t('Language'),
      'bundle' => t('Content type'),
      'field' => t('Field'),
      'currentString' => t('Current string'),
      'stringReplacement' => t('String replacement'),
    ];

    //Data is left in the session by the batch process
    $data  = $this->tempStoreFactory->get('text_replace')->get('batch_results')['results'];
    $pre_processed = $this->buildTableData($data, $form_state->get('string_replacement'));

    //Clean session data after using it. Avoids inconsistencies.
   // unset($_SESSION['text_replace.batch_results']);
    $form_state->unsetValue('string_replacement')->setRebuild(TRUE);


    //Preserve preprocessed so we speed up submit handling.
    //Clean unused memory.
    $form_state->set('batch', NULL);

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $pre_processed,
      '#empty' => t('No suitable nodes found.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      // Custom submission handler for 'Back' button.
      '#submit' => ['::submitBackButton'],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Replace'),
      '#submit' => ['::submitPageFour'],
    ];

    if (empty($pre_processed)) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * Provides custom submission handler for page 3.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitPageFour(array &$form, FormStateInterface $form_state) {
    $elementSelected = $form_state->getValue('table');
    $stringReplacement = $form_state->get('string_replacement');
    $currentString = $form_state->get('current_string');

    $contentTypes = [];
    foreach ($elementSelected as $element) {
      if ($element != 0){
        $nid = explode('.',$element)[0];
        $type = $this->controller->getEntityNodeByNid($nid)->getType();
        $contentTypes[$type] = $type;
      }
    }

    $this->tempStoreFactory->get('text_replace')->set('elementsSelected', $elementSelected);
    $this->tempStoreFactory->get('text_replace')->set('strings', ['currentString' => $currentString, 'stringReplacement' => $stringReplacement]);
    $this->tempStoreFactory->get('text_replace')->set('contentTypes', $contentTypes);

    $form_state->setRedirect('text_replace.confirm_update_form');
    return;


    //Clean unneeded memory
    $form_state->set('page_values', NULL);
    $form_state->set('pre_processed_nodes', NULL);
    $form_state->set('table', NULL);
    $form_state->set('string_replacement', NULL);
    $form_state->set('current_string', NULL);
    $form_state->cleanValues();

    $this->moveNextPage($form_state);
  }


  /**
   * Pre-process (flattens) data from service to be shown in the table.
   *
   * @param array $data
   *
   * @return array
   */
  protected function buildTableData(array $data, $stringReplacement) {
    $table = [];


    foreach ($data as $key => $content_type) {
      foreach ($content_type as $nid) {
        foreach ($nid as $lang_code) {
          foreach ($lang_code as $translation) {
            $nid = $translation['nid'];
            $lang_code = $translation['lang_code'];
            $field = $translation['field'];
            $tableKey = "$nid.$lang_code.$field";
            $table[$tableKey] = [
              'nid' => $nid,
              'title' => $this->controller->getEntityNodeByNid($nid)
                ->getTitle(),
              'lang_code' => $lang_code,
              'bundle' => $key,
              'field' => $field,
              'currentString' => $translation['currentString'],
              'stringReplacement' => $stringReplacement,
            ];
          }
        }
      }
    }

    return $table;
  }

  /**
   * Pre-process (flattens) data from service to be shown in the table.
   *
   * @param array $data
   *
   * @return array
   */
  protected function buildTableData2(array $data) {
    $table = [];

    foreach ($data as $node) {
      foreach ($node[key($node)] as $field) {
        $required = ($field->isRequired() == 1) ? t('TRUE') : t('FALSE');
        $table[$field->id()] = [
          'bundle' => key($node),
          'fid' => $field->id(),
          'field_name' => $field->getName(),
          'field_type' => $field->getType(),
          'label' => $field->label(),
          'required' => $required,
        ];
      }
    }

    return $table;
  }

  /**
   * Handles the Back button action
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitBackButton(array &$form, FormStateInterface $form_state) {
    $this->movePrevPage($form_state);
  }

  /**
   * Generates checkboxes options.
   *
   * @return array
   */
  public function getOptionsCheckbox() {
    $nameContentTypes = [];
    $contentTypes = $this->controller->getContentTypes();
    foreach ($contentTypes as $machine_name => $type) {
      $nameContentTypes[$machine_name] = $type->label();
    }
    return $nameContentTypes;
  }

  /**
   * Processes user input on checkboxes.
   *
   * @param $list_content
   *
   * @return array
   */
  public function processCheckboxSelected($list_content) {
    $result = [];
    foreach ($list_content as $keys => $content) {
      if ($content !== 0) {
        $result[$keys] = $keys;
      }
    }
    return $result;
  }


  /**
   * Rebuilds the form to the next page
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected function moveNextPage(FormStateInterface $form_state) {
    $current_page = $form_state->get('page_num');

    $form_state
      ->set('page_num', ++$current_page)
      ->setRebuild(TRUE);
  }

  /**
   * Rebuilds the form to the previous page
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected function movePrevPage(FormStateInterface $form_state) {
    $current_page = $form_state->get('page_num');

    $form_state
      ->set('page_num', --$current_page)
      ->setRebuild(TRUE);
  }


  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $
   * form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //Need to be implemented because its defined abstract in parent. Not needed.
  }
}
